package mia.uci.cu.clase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private EditText etNum1, etNum2;
    private RadioButton rbSuma, rbResta;
    private TextView tvResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etNum1 = (EditText) findViewById(R.id.et1);
        etNum2 = (EditText) findViewById(R.id.et2);

        rbSuma = (RadioButton) findViewById(R.id.rbSuma);
        rbResta = (RadioButton) findViewById(R.id.rbResta);

        tvResultado = (TextView) findViewById(R.id.result);
    }

    public void calcular(View view){
        Integer numero1 = Integer.parseInt(etNum1.getText().toString());
        Integer numero2 = Integer.parseInt(etNum2.getText().toString());

        if (rbSuma.isChecked())
            tvResultado.setText(String.valueOf(numero1+numero2));
        else
            tvResultado.setText(String.valueOf(numero1-numero2));
    }
}